#!/bin/bash

# 1. Install python and nginx
sudo apt update -y
sudo apt upgrade -y
sudo apt install -y python3-pip python3-dev python3.8-venv libmysqlclient-dev libjpeg8-dev libpng-dev libfreetype6-dev nginx

# 2. Set passwords for database
ROOT_PASS="#d8fQjE^SZ6^g^+X"
DB_USER_PASS='9hb#8NV/)dnmKQP'
DB_USER_NAME='bienfacile'
ENV_NAME='venv'
WHOAMI=`whoami`

# 3. Configure root pass and install mysql
echo 'Installing mysql'
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $ROOT_PASS"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $ROOT_PASS"
sudo apt-get -y install mysql-server

# 4. create bienfacile user, database
MYSQL=`which mysql`
#$MYSQL --user=root --password=$ROOT_PASS -e "CREATE DATABASE IF NOT EXISTS bienfacile_agency; GRANT ALL PRIVILEGES ON bienfacile_agency.* TO '$DB_USER_NAME'@'localhost' IDENTIFIED BY `$DB_USER_PASS`;FLUSH PRIVILEGES;"
$MYSQL --user=root --password=$ROOT_PASS -f -e "CREATE DATABASE IF NOT EXISTS bienfacile_agency;CREATE USER '$DB_USER_NAME'@'localhost' IDENTIFIED BY '$DB_USER_PASS';GRANT ALL PRIVILEGES ON  *.* to '$DB_USER_NAME'@'localhost';"

# 5. create virtual_environment
echo 'setup virtualenv'
python3 -m venv /home/$WHOAMI/projects/$ENV_NAME

source /home/$WHOAMI/projects/$ENV_NAME/bin/activate
pip install -U pip
pip install -r ../requirements_v4.txt
python ../manage.py migrate

# 6. Import prod database
$MYSQL --user=$DB_USER_NAME --password=$DB_USER_PASS bienfacile_agency < /home/$WHOAMI/projects/bienfacile-2021-12-02.sql

# 7. migrate oauth2_provider
python ../manage.py migrate oauth2_provider --fake
python ../manage.py makemigrations
python ../manage.py migrate

# createsuperuser
python ../manage.py createsuperuser
#python manage.py runserver

# 8. Setup gunicorn
echo 'setup gunicorn'
#WHOAMI=`whoami`
DJANGO_PROJECT_NAME='bienfacile-agency-e8d2cee05d69'

# 10. Create gunicorn systemd service
sudo rm /etc/systemd/system/gunicorn_new.service
sudo echo "
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=$WHOAMI
Group=www-data
WorkingDirectory=/home/$WHOAMI/projects/$DJANGO_PROJECT_NAME
#EnvironmentFile=/home/shadowfox/secrets.txt
ExecStart=/home/$WHOAMI/projects/$ENV_NAME/bin/gunicorn --access-logfile - --workers 3 --bind unix:/home/$WHOAMI/projects/$DJANGO_PROJECT_NAME/bienfacile.sock agency.wsgi:application

[Install]
WantedBy=multi-user.target
" | sudo tee --append /etc/systemd/system/gunicorn_new.service

# 11. Start gunicorn service
sudo systemctl start gunicorn_new
sudo systemctl enable gunicorn_new

#sudo systemctl daemon-reload
sudo systemctl restart gunicorn_new


# 12. Create Nginx template
sudo mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
echo 'setup Nginx'
sudo echo "
server {
    listen 80;
    server_name 127.0.0.1;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/$WHOAMI/projects/$DJANGO_PROJECT_NAME;
    }

    location /media/ {
        root /home/$WHOAMI/projects/$DJANGO_PROJECT_NAME;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/$WHOAMI/projects/$DJANGO_PROJECT_NAME/bienfacile.sock;
    }
}" | sudo tee --append /etc/nginx/sites-available/bienfacile_new


sudo ln -s /etc/nginx/sites-available/bienfacile_new /etc/nginx/sites-enabled

sudo nginx -t

sudo systemctl restart nginx
