"""
Django settings for agency project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
from urllib.parse import urlparse

import dj_database_url
from django.core.management.utils import get_random_secret_key

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("DJANGO_SECRET_KEY", get_random_secret_key())

AWS_ACCESS_KEY_ID = 'AKIAIADEAKOANSKRCTGQ'
AWS_SECRET_ACCESS_KEY = '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'

AWS_STORAGE_BUCKET_NAME = 'uploads.bienfacile.com'
AWS_S3_REGION_NAME = 'eu-west-3'
AWS_S3_ENDPOINT_URL = 'https://s3-eu-west-3.amazonaws.com'

S3DIRECT_DESTINATIONS = {
    'uploads': {
        'key': lambda filename, args: args + '/' + filename,
        'key_args': 'uploads',
        'content_disposition': lambda x: 'attachment; filename="{}"'.format(x),
        'content_length_range': (1000, 20000000),
    }
}


# SECURITY WARNING: don't run with debug turned on in production!
SITE_URLS = { 'DEV' : 'http://192.168.1.72:8000', 'STAGING' : 'https://staging.bienfacile.com', 'LIVE' : 'https://agence.bienfacile.com' }

SERVER_ENVIRONMENT = os.environ['SERVER_ENVIRONMEMT'] if 'SERVER_ENVIRONMEMT' in os.environ else 'LIVE'
DEVELOPMENT_MODE = os.getenv("DEVELOPMENT_MODE", "False") == "True"

# SERVER_ENVIRONMENT = 'DEV'

# SERVER_ENVIRONMENT ='LIVE'

DEBUG = os.getenv("DEBUG", False)
# DEBUG = True if SERVER_ENVIRONMENT != 'LIVE' else False

SITE_URL = SITE_URLS[SERVER_ENVIRONMENT]

#LOGGING_CONFIG = None

SETTINGS_EXPORT = [
    'DEBUG',
    'MEDIA_URL',
    'SERVER_ENVIRONMENT',
    'SITE_URL',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': ['agents/templates','venv/lib/python2.7/site-packages/django/contrib/admin/templates'],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django_settings_export.settings_export',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ALLOWED_HOSTS = os.getenv("DJANGO_ALLOWED_HOSTS","agence.bienfacile.com,staging.bienfacile.com,demo.bienfacile.com,0.0.0.0,127.0.0.1,192.168.1.76").split(",")

# Application definition

INSTALLED_APPS = [
    # 'grappelli',
    'widget_tweaks',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'user_sessions',
    'django.contrib.staticfiles',
    'oauth2_provider',
    'corsheaders',
    's3direct',
    'agents',
    'notaires',
    'clients',
    'todo',
    'chat',
    'news',
    'sales',
    'simulations',
    'files',
    'finance',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    # 'user_sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
#    'agency.MobileMiddleware.MobileMiddleware',
       ]
CORS_ORIGIN_ALLOW_ALL = True

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'oauth2_provider.backends.OAuth2Backend',
)

SESSION_ENGINE = 'user_sessions.backends.db'

SESSION_COOKIE_AGE = 2592000 # expire after 30 days

AUTH_USER_MODEL = 'agents.Agent'

GEOIP_PATH = '/home/sites/django/bienfacile/marchand/geoip/'

ROOT_URLCONF = 'agency.urls'

WSGI_APPLICATION = 'agency.wsgi.application'

LOGIN_REDIRECT_URL = 'otp'

LOGIN_URL = 'login'

LOGOUT_URL = 'login'


# DATABASE_URL1= "mysql://doadmin:cvsWgUYcvrhY3ls0@db-mysql-nyc3-46902-do-user-10468321-0.b.db.ondigitalocean.com:25060/defaultdb"

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
if DEVELOPMENT_MODE is True:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": os.path.join(PROJECT_DIR, "db.sqlite3"),
        }
    }
elif len(sys.argv) > 0 and sys.argv[1] != 'collectstatic':
    if os.getenv("DATABASE_URL", None) is None:
        raise Exception("DATABASE_URL environment variable not defined")
    DATABASES = {
        "default": dj_database_url.parse(os.environ.get("DATABASE_URL")),
    }

# DATABASES = {

    # "default": {
    #     "ENGINE": "django.db.backends.sqlite3",
    #     "NAME": os.path.join(PROJECT_DIR, "db.sqlite3")
    # }

    # 'default': {
    # 'ENGINE': 'django.db.backends.mysql',
    # 'NAME' : 'defaultdb',
    # 'USER': 'doadmin',
    # 'PASSWORD': 'cvsWgUYcvrhY3ls0',
    # 'HOST':'db-mysql-nyc3-46902-do-user-10468321-0.b.db.ondigitalocean.com',
    # 'PORT':25060
    #     }
# }

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Media files - this includes file uploads

MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media/')
MEDIA_URL = '/media/'

# Static files (CSS, JavaScript, Images)

STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')
STATIC_URL = '/static/' if DEBUG else 'https://agencystatic.bienfacile.com/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'staticfiles'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)
