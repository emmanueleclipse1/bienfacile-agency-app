from django.contrib import admin
from django.urls import include, re_path, path
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.contrib.auth.views import logout_then_login
from django.views.static import serve as serve_static
from django.conf import settings
from agents.views import HomePageView, PreferencesView, HelpView, AnnoncesView, AgencesView, AgenceView, AgentsView, UpdateAgenciesView, json_agences, json_agents, json_monagence, user_login, verify_otp
from clients.views import ClientsView, ClientView, BuyersView, SellersView, ClientLeadsView, json_contacts_client, json_buyers, json_sellers, json_recent_activities, json_clients, json_clientleads, csv_clientleads, json_addclientlead, contactform
from simulations.views import SimulationMortgageView, SimulationCapitalGainsView, SimulationSeasonalRentalView
from finance.views import SommaireFinanceView, SuppliersView, json_suppliers, SuppliersFactureView, json_suppliers_facture, SuppliersFactureView, BankAccountView, json_banks, json_bankentries, json_bankupload, NotairesFactureView, json_notaire_factures, AgentsFactureView, json_agent_factures, ApporteursFactureView, json_apporteur_factures
from todo.views import ToDoView,json_todo
from chat.views import ChatView,GroupChatView,json_chat,json_unreadchat,json_listchat,json_groupchat
from news.views import json_news
from property.views import MandatsView
import oauth2_provider.views as oauth2_views
from django.contrib.auth import views as auth_views 

admin.autodiscover()

# OAuth2 provider endpoints
oauth2_endpoint_views = [
    re_path(r'^authorize/$', oauth2_views.AuthorizationView.as_view(), name="authorize"),
    re_path(r'^token/$', oauth2_views.TokenView.as_view(), name="token"),
    re_path(r'^revoke-token/$', oauth2_views.RevokeTokenView.as_view(), name="revoke-token"),
]

urlpatterns = [
    # re_path(r'^grappelli/', include('grappelli.re_paths')),
	re_path(r'^login/$', user_login, name='login'),
	path('verifyotp/<int:id>/', verify_otp, name='otp'),
	re_path(r'logout/', auth_views.LogoutView.as_view(), name='logout'),
	re_path(r'^$', login_required(HomePageView.as_view()), name='home'),

	re_path(r'^profile/$', login_required(PreferencesView.as_view())),
	re_path(r'^help/$', login_required(HelpView.as_view())),
	re_path(r'^annonces/(?P<days>[0-9]+)/$', login_required(AnnoncesView.as_view())),
	re_path(r'^agences/$', login_required(AgencesView.as_view())),
	re_path(r'^monagence/$', login_required(AgenceView.as_view())),
	re_path(r'^agence/$', login_required(AgenceView.as_view())),
	re_path(r'^agents/$', login_required(AgentsView.as_view())),
	re_path(r'^updateagencies/$', login_required(UpdateAgenciesView.as_view())),

	re_path(r'^clients/$', login_required(ClientsView.as_view())),
	re_path(r'^client/$', login_required(ClientView.as_view())),
	re_path(r'^client/(?P<clientid>[0-9]+)/$', login_required(ClientView.as_view())),
	re_path(r'^acheteurs/$', login_required(BuyersView.as_view())),
	re_path(r'^vendeurs/$', login_required(SellersView.as_view())),
	re_path(r'^clientleads/$', login_required(ClientLeadsView.as_view())),

	re_path(r'^taches/$', login_required(ToDoView.as_view())),

	re_path(r'^chat/$', login_required(ChatView.as_view())),
	re_path(r'^groupchat/$', login_required(GroupChatView.as_view())),

	re_path(r'^simulation-credit/$', login_required(SimulationMortgageView.as_view())),
	re_path(r'^simulation-plusvalue/$', login_required(SimulationCapitalGainsView.as_view())),
	re_path(r'^simulation-saisonniere/$', login_required(SimulationSeasonalRentalView.as_view())),

	re_path(r'^suppliers/$', login_required(SuppliersView.as_view())),
	re_path(r'^suppliers_facture/$', login_required(SuppliersFactureView.as_view())),
	re_path(r'^banque/$', login_required(BankAccountView.as_view())),
	re_path(r'^facture_notaire/$', login_required(NotairesFactureView.as_view())),
	re_path(r'^facture_agent/$', login_required(AgentsFactureView.as_view())),
	re_path(r'^facture_apporteur/$', login_required(ApporteursFactureView.as_view())),
	re_path(r'^sommaire_finance/$', login_required(SommaireFinanceView.as_view())),

	re_path(r'^json_agences/$', login_required(json_agences)),
	re_path(r'^json_agents/$', login_required(json_agents)),
	re_path(r'^json_monagence/$', login_required(json_monagence)),
	re_path(r'^json_clients/$', login_required(json_clients)),
	re_path(r'^json_contacts_client/$', login_required(json_contacts_client)),
	re_path(r'^json_buyers/$', login_required(json_buyers)),
	re_path(r'^json_sellers/$', login_required(json_sellers)),
	re_path(r'^json_recent_activities/$', login_required(json_recent_activities)),
	re_path(r'^json_todo/$', login_required(json_todo)),
	re_path(r'^json_chat/$', login_required(json_chat)),
	re_path(r'^json_unreadchat/$', login_required(json_unreadchat)),
	re_path(r'^json_listchat/$', login_required(json_listchat)),
	re_path(r'^json_groupchat/$', login_required(json_groupchat)),
	re_path(r'^json_news/$', login_required(json_news)),
	re_path(r'^json_clientleads/$', login_required(json_clientleads)),
	re_path(r'^csv_clientleads/$', login_required(csv_clientleads)),
	re_path(r'^json_addclientlead/$', login_required(json_addclientlead)),
	re_path(r'^json_suppliers/$', login_required(json_suppliers)),
	re_path(r'^json_suppliers_facture/$', login_required(json_suppliers_facture)),
	re_path(r'^json_banks/$', login_required(json_banks)),
	re_path(r'^json_bankentries/$', login_required(json_bankentries)),
	re_path(r'^json_bankupload/$', login_required(json_bankupload)),
	re_path(r'^json_notaire_factures/$', login_required(json_notaire_factures)),
	re_path(r'^json_agent_factures/$', login_required(json_agent_factures)),
	re_path(r'^json_apporteur_factures/$', login_required(json_apporteur_factures)),

	re_path(r'^mandats/$', login_required(MandatsView.as_view())),

	re_path(r'^contactform/$', contactform),

	re_path(r'', include('user_sessions.urls', 'user_sessions')),
	re_path(r'^admin/', admin.site.urls),
	# re_path('accounts/', include('django.contrib.auth.urls')),
	# re_path(r'^accounts/login/$', login, {'template_name': 'login.html'}, name="login"),
	# re_path(r'^accounts/logout/$', logout_then_login),
	re_path(r'^static/(?P<path>.*)$', serve_static, {'document_root' : settings.STATIC_ROOT} ),

	re_path(r'^o/', include(oauth2_endpoint_views)),
	re_path(r'^s3direct/', include('s3direct.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
	# OAuth2 Application Management endpoints
	oauth2_endpoint_views += [
		re_path(r'^applications/$', oauth2_views.ApplicationList.as_view(), name="list"),
		re_path(r'^applications/register/$', oauth2_views.ApplicationRegistration.as_view(), name="register"),
		re_path(r'^applications/(?P<pk>\d+)/$', oauth2_views.ApplicationDetail.as_view(), name="detail"),
		re_path(r'^applications/(?P<pk>\d+)/delete/$', oauth2_views.ApplicationDelete.as_view(), name="delete"),
		re_path(r'^applications/(?P<pk>\d+)/update/$', oauth2_views.ApplicationUpdate.as_view(), name="update"),
	]

	# OAuth2 Token Management endpoints
	oauth2_endpoint_views += [
		re_path(r'^authorized-tokens/$', oauth2_views.AuthorizedTokensListView.as_view(), name="authorized-token-list"),
		re_path(r'^authorized-tokens/(?P<pk>\d+)/delete/$', oauth2_views.AuthorizedTokenDeleteView.as_view(),
			name="authorized-token-delete"),
	]

	urlpatterns += [
		re_path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
	]


