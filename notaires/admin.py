from django.contrib import admin
from notaires.models import CabinetNotaire,Notaire

class CabinetNotaireAdmin(admin.ModelAdmin):
    list_display = ('name', 'telephone', 'email', 'address','city',)
    search_fields = ['name']
admin.site.register(CabinetNotaire, CabinetNotaireAdmin)

class NotaireAdmin(admin.ModelAdmin):
    list_display = ('forename', 'email', 'phone', 'mobile', 'cabinet',)
admin.site.register(Notaire, NotaireAdmin)
