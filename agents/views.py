# -*- coding: utf-8 -*-
import json
import string
import random
import os

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login,load_backend
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.views.generic import View
from datetime import datetime, timedelta
from django.utils import timezone
from django.shortcuts import render
from django.db.models import Q

from .models import OTP
from .forms import LoginFormPassword,LoginFormOPT, OTPForm

from twilio.rest import Client

from agents.models import Agency,Agent, AgentSettings
from agency.utils import JsonException, pkobject, editobject, setobject
import scraper.rabbitutil as queueutil

account_sid = "ACb7e53137d0619521dbe281417bcfd577"
auth_token = "48119eb9061223b2d92841e7161309bd"
account_phone = "+19286429166"


class HomePageView(TemplateView):
    template_name = "index.html"

class AnnoncesView(TemplateView):
    template_name = "annonces.html"

class PreferencesView(TemplateView):
    template_name = "preferences.html"

class HelpView(TemplateView):
    template_name = "help.html"

class AgencesView(TemplateView):
    template_name = "agences.html"

class AgenceView(TemplateView):
    template_name = "agence.html"

class AgentsView(TemplateView):
    template_name = "agents.html"

def json_agences(request):
    agencies = Agency.objects.all().order_by('name')
    data = []
    for agency in agencies:
        data.append({ 'name': agency.agencyname, 'branch': agency.branch, 'address': agency.address, 'city': agency.city, 'postcode': agency.postcode, 'telephone': agency.telephone, 'email': agency.email, 'web': agency.web, 'search': agency.search, 'id': agency.id })
    return JsonResponse(data, safe=False)

def json_agents(request):
    if request.method == 'POST' and request.user.is_admin:
        fields = [
                    { 'name': 'forename', 'required': True },
                    { 'name': 'surname', 'required': True },
                    { 'name': 'role', },
                    { 'name': 'mobile', },
                    { 'name': 'public', 'type': 'boolean' },
                ]
        try:
            # Get if ID or create
            pk = pkobject(request.POST)
            item, created = Agent.objects.filter(pk=pk).get_or_create() if pk else (Agent(), False)
            if editobject(request.POST, item, created):
                # Extra modifications to the object here

                # Set fields and save
                if setobject(request.POST, item, fields):
                    item.save()
        except Exception as e:
            return JsonException(e)
    agents = Agent.objects.all().order_by('surname') if request.user.is_admin else Agent.objects.filter(Q(public=True)|Q(agency=request.user.agency)).order_by('surname')
    print(len(agents))
    data = []
    for agent in agents:
        print(agent)
        time_ago = timezone.now() - timedelta(days=90)
        data.append({ 'id' : agent.id, 'public' : agent.public, 'forename': agent.forename, 'surname': agent.surname, 'agency': agent.agency.name, 'branch': agent.agency.branch, 'mobile': agent.mobile, 'email': agent.email, 'search': agent.search, 'agency_id': agent.agency_id, 'isonline' : False if not agent.last_login or agent.last_login < time_ago else True })
    return JsonResponse(data, safe=False)

def json_monagence(request):
    agents = Agent.objects.filter(agency=request.user.agency).order_by('surname')
    data = []
    for agent in agents:
        thumbnail = agent.thumbnail() if agent.photo else None
        data.append({ 'forename': agent.forename, 'surname': agent.surname, 'agency': agent.agency.agencyname, 'branch': agent.agency.branch, 'mobile': agent.mobile, 'email': agent.email, 'thumbnail': thumbnail, 'search': agent.search, 'agency_id': agent.agency_id, 'id' : agent.id })
    return JsonResponse(data, safe=False)

def get_usersetting(agent):
    result = {}
    settings = AgentSettings.objects.filter(agent=agent)
    for setting in settings:
        result[setting.key] = setting.value
    return result

def set_usersetting(agent,settings):
    for key, value in settings.items():
        setting, created = AgentSettings.objects.get_or_create(key=key,agent=agent)
        if not value or value == '':
            setting.delete()
        else:
            setting.value = value
            setting.save()

class UpdateAgenciesView(TemplateView):
    template_name = "updateagencies.html"

    def get_context_data(self, **kwargs):
        context = super(UpdateAgenciesView, self).get_context_data(**kwargs)
        context['usersettings'] = get_usersetting(self.request.user)
        return context

    def agentline(self, details):
        result = details.forename+' '+details.surname+', '
        result += ('numero portable '+details.mobile if details.mobile else "besoin de son numero portable")
        result += ', '+('email '+details.email if details.email else ", besoin de son adresse email")
        return result + "\n"

    def post(self, request, *args, **kwargs):
        if request.POST.get('save', None):
            result = "Saved"
        else:
            agencies = Agency.objects.all().order_by('id')
            agentslist = Agent.objects.all().order_by('agency')
            agents = {}
            emails = []
            sendlist = []
            i=0
            result = ''
            for item in agentslist:
                if item.agency_id in agents:
                    agents[item.agency_id].append(item)
                else:
                    agents[item.agency_id] = [item, ]
            queue = queueutil.ScraperQueue()
            for item in agencies:
                agency = item.name+(' ('+item.branch+')' if item.branch else '')
                agency = (item.prefix+' ' if item.prefix else '')+item.name+(' '+item.postfix if item.postfix else '')

                email = item.email
                if email not in emails:
                    numagents = len(agents[item.id]) if item.id in agents else 0
                    result += '<br/>'+str(i)+') Agency: '+agency+' has '+(str(numagents) if numagents else 'no')+' agents'
                    subject = request.POST.get('subject', 'Email pour')+' '+agency
                    body = ''
                    if numagents == 0:
                        body = request.POST.get('none', '')
                    if numagents == 1:
                        body = request.POST.get('one', '')+"\n\n"
                        body += self.agentline(agents[item.id][0])
                    if numagents > 1 or numagents == 1 and body == '':
                        body = request.POST.get('multiple', '')+"\n\n"
                        for agentdetails in agents[item.id]:
                            body += self.agentline(agentdetails)
                    if body != '':
                        body += "\n\n"+request.POST.get('signature', '')
                        envelope = { 'to' : (agency+' <'+email+'>'), 'from': 'Phillip Temple <phillip@paradiseproperties.fr>', 'body' : body, 'subject' : subject }
                        result += '<hr/><table><tr><td>To: '+envelope['to']+'</td><td>From: '+envelope['from']+'</td><td>Subject: '+envelope['subject']+'</td></tr></table><br/><pre>'+envelope['body']+'</pre><hr/>'
                        if request.POST.get('send', ''):
                            queue_name = 'mails'
                            message = json.dumps(envelope)
                            queue.ensureopen(queue_name)
                            queue.send(queue_name,message)

                    emails.append(email)

                else:
                    result += '<br/>Ignoring duplicate email '+email

                i += 1
            set_usersetting( request.user, { 'updateagencies_lastupdate' : str(datetime.now()), } )

        set_usersetting( request.user, {  'updateagencies_none' : request.POST.get('none', None),
                            'updateagencies_one' : request.POST.get('one', None),
                            'updateagencies_multiple' : request.POST.get('multiple', None),
                            'updateagencies_subject' : request.POST.get('subject', None),
                            'updateagencies_signature' : request.POST.get('signature', None),
                        } )
        return HttpResponse(result)


def get_code():
    """
    Function that generates an OTP of 6 characters and  numbers.
    """
    S = 6
    otp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=S))
    return otp


client = Client(account_sid, auth_token)


def send_otp(phone_number, otp):
    """
    Function to send OTP to client phone numbers.
    phonenumber: the phone number of the client
    otp: generated OTP
    """
    # phone_number = '+' + phone_number
    print('sending ', otp, ' to ', phone_number)
    print('OWNER PHONE', account_phone)
    client.messages.create(
        body=otp,
        from_=account_phone,
        to=phone_number
    )


def user_login(request):
    """
    Function to authenticate the client based on credentials provided
    """
    if request.method == 'POST':
        form1 = LoginFormPassword(request.POST)
        form2 = LoginFormOPT(request.POST)
        if request.user_agent.is_mobile:
            if form2.is_valid():
                cd = form2.cleaned_data
                mobile = cd['phonenumber'].strip()
                if (not "+" in mobile):
                    mobile = '+' + mobile
                format_1 = mobile[:4]+" "+mobile[4:6]+" "+mobile[6:9] +" "+mobile[9:len(mobile)]
                format_2 = mobile[:3] + " " + mobile[3:6] + " " + mobile[6:9] + " " + mobile[9:len(mobile)]

                user = Agent.objects.filter(mobile__in=[mobile,format_1,format_2])

                if user is not None and len(user)==1:
                    user = user[0]
                    user_id = user.id
                    if not hasattr(user, 'backend'):
                        for backend in settings.AUTHENTICATION_BACKENDS:
                            if user == load_backend(backend).get_user(user.pk):
                                user.backend = backend
                                break
                    if user.is_active:
                        otp = get_code()
                        OTP.objects.create(user=user, otp_code=otp)
                        send_otp(mobile, otp)
                        return redirect('otp', id=user_id)
                    else:
                        return HttpResponse('Disabled account')
                else:
                    messages.error(request, 'phone number not found')
                    return render(request, 'login.html', {'form': form2})


        else:
            if form1.is_valid():
                cd = form1.cleaned_data

                # check with user_name and password only
                user = authenticate(request, username=cd['username'], password=cd['password'])
                if user is not None:
                    login(request,user)
                    return redirect('home')
                else:
                    messages.error(request, 'username or password not correct')
                    return render(request, 'login.html', {'form': form1})
            else:
                return render(request, 'login.html', {'form': form1})
    elif request.method == 'GET' and request.user_agent.is_mobile:
        form = LoginFormOPT()
        return render(request, 'login.html', {'form': form})
    elif request.method == 'GET' and request.user_agent.is_mobile == False:
        form = LoginFormPassword()
        return render(request, 'login.html', {'form': form})


def verify_otp(request, id):
    """
    Function to verify OTP sent to client
    """
    if request.method == 'POST':
        form = OTPForm(request.POST)
        agent = Agent.objects.get(id=id)
        if form.is_valid():
            otp_code = form.cleaned_data['otp_code']
            otp_count = OTP.objects.filter(user=agent, otp_code=otp_code).count()
            if otp_count > 0:
                otp = OTP.objects.filter(user=agent).last()
                if str(otp) == otp_code:
                    login(request, agent, backend='django.contrib.auth.backends.ModelBackend')
                    otp.delete()
                    return redirect('home')
                else:
                    return redirect('login')
            else:
                return redirect('login')
    else:
        form = OTPForm()
        return render(request, 'otp.html', {'form': form})
