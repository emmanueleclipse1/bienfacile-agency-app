from django.contrib.auth.backends import ModelBackend

from agents.models import Agent


class PasswordlessAuthBackend(ModelBackend):
    """Log in to Django without providing a password.

    """
    def authenticate(self, username=None):
        try:
            return Agent.objects.get(username=username)
        except Agent.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Agent.objects.get(pk=user_id)
        except Agent.DoesNotExist:
            return None