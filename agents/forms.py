from django import forms

from .models import OTP

class LoginFormPassword(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class LoginFormOPT(forms.Form):
    phonenumber = forms.CharField()


class OTPForm(forms.ModelForm):
    otp_code = forms.CharField(max_length=6)

    class Meta:
        model = OTP
        # fields="__all__"
        fields = ['otp_code']